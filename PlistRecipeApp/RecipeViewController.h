//
//  RecipeViewController.h
//  PlistRecipeApp
//
//  Created by Matthew Griffin on 5/31/15.
//  Copyright (c) 2015 Matthew Griffin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RecipeViewController : UIViewController

@property(nonatomic, strong)
NSDictionary* RecipeHolder;

@end
