//
//  RecipeViewController.m
//  PlistRecipeApp
//
//  Created by Matthew Griffin on 5/31/15.
//  Copyright (c) 2015 Matthew Griffin. All rights reserved.
//

#import "RecipeViewController.h"

@interface RecipeViewController ()

@end

@implementation RecipeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    float width = self.view.frame.size.width;
    float height = self.view.frame.size.height;
    
    self.view.backgroundColor = [UIColor blackColor];
    
    //Show the Recipe title
    UILabel* lbl = [[UILabel alloc]initWithFrame:CGRectMake(20, height/10, width - 40, height/20)];
    lbl.textColor = [UIColor whiteColor];
    lbl.text = [self.RecipeHolder objectForKey:@"name"];
    lbl.backgroundColor = [UIColor orangeColor];
    lbl.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:lbl];
    
    //Show the recipe ingredients
    UITextView*  textblock = [[UITextView alloc]initWithFrame:CGRectMake(20, ((height/4)*3) - 100 , (width - 40), 100)];
    textblock.text = [self.RecipeHolder objectForKey:@"Ingredients"];
    textblock.backgroundColor =[UIColor purpleColor];
    textblock.textColor = [UIColor whiteColor];
    textblock.editable = NO;
    [self.view addSubview:textblock];
    
    //Show the recipe directions
    UITextView* instructiontextblock = [[UITextView alloc]initWithFrame:CGRectMake(20, (height/5)*4 , (width - 40), 100)];
    instructiontextblock.text = [self.RecipeHolder objectForKey:@"Instructions"];
    instructiontextblock.backgroundColor =[UIColor orangeColor];
    instructiontextblock.textColor = [UIColor whiteColor];
    instructiontextblock.editable = NO;
    [self.view addSubview:instructiontextblock];
    
    //Show the recipe picture
    UIImageView* image = [[UIImageView alloc]initWithFrame:CGRectMake(20, (height/6), width - 40, height/2.5)];
    image.contentMode = UIViewContentModeScaleAspectFit;
    [image setImage:[UIImage imageNamed:[self.RecipeHolder objectForKey:@"image"]]];
    [self.view addSubview:image];
    
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
