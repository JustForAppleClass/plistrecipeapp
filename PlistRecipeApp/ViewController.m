//
//  ViewController.m
//  PlistRecipeApp
//
//  Created by Matthew Griffin on 5/31/15.
//  Copyright (c) 2015 Matthew Griffin. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()



@end

@implementation ViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
   
    
    
    UITableView* tableview = [[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStylePlain];
    tableview.delegate = self;
    tableview.dataSource = self;
    [self.view addSubview:tableview];
}



-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"cell" ];
    if(!cell)
    {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
       
                                       
                                       
    }
    NSString* filepath = [[NSBundle mainBundle]pathForResource:@"RecipeList" ofType:@"plist"];
    NSArray* arrayofRecipes = [[NSArray alloc]initWithContentsOfFile:filepath];
    NSDictionary* dictionaryRec = [NSDictionary dictionaryWithDictionary:arrayofRecipes[indexPath.row]];
                                   cell.textLabel.text = dictionaryRec[@"name"];
                                       return cell;
                                       
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSString* filepath = [[NSBundle mainBundle]pathForResource:@"RecipeList" ofType:@"plist"];
    NSArray* arrayofRecipes = [[NSArray alloc]initWithContentsOfFile:filepath];
    return arrayofRecipes.count;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
                                       {
                                           [tableView deselectRowAtIndexPath:indexPath animated:YES];
                                           NSString* filepath = [[NSBundle mainBundle]pathForResource:@"RecipeList" ofType:@"plist"];
                                           NSArray* arrayofRecipes = [[NSArray alloc]initWithContentsOfFile:filepath];
                                           RecipeViewController* recipe = [RecipeViewController new];
                                           recipe.RecipeHolder = arrayofRecipes[indexPath.row];
                                           [self.navigationController pushViewController:recipe animated:YES];
                                       }

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
